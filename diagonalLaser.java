import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class diagonalLaser extends Actor
{
    boolean directionSet = false;
    
    public void act()
    {
        if (directionSet == false) {
            if (getX() >= 200) {
                setRotation(30);
            } else {
                setRotation(160);
            }
            directionSet = true;
        }
        
        //0 degrees = to the right
        //turns around when reaching the edge
        if (getX() < 6) {
            setRotation(30);
        }
        
        if (getX() > 394) {
            setRotation(160);
        }
        
        move(4);
        
        if (isAtEdge() == true) {
                    //removes object from world if touching the edge
                    getWorld().removeObject(this);
        }
        
    }
}
