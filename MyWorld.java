import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(400, 600, 1);
        
        //layer order - first comes on top
        setPaintOrder(player.class, HPbar.class, scorebar.class, gameOverScreen.class, powerup.class);
        // Spawns the player character at this position
        addObject(new player(), 200, 500);
        addObject(new director(), 200, 10);
        addObject(new HPbar(), 330, 30);
        addObject(new scorebar(), 70, 30);
        addObject(new shieldEffect(), 100, 100);
        //addObject(new boss(),200, 1);
        //syntax: addObject(new Actor(), X, Y);
        
    }
}
