import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)

/**
 * Ergänzen Sie hier eine Beschreibung für die Klasse playerLaser.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class playerLaser extends Actor
{
    /**
     * Act - tut, was auch immer playerLaser tun will. Diese Methode wird aufgerufen, 
     * sobald der 'Act' oder 'Run' Button in der Umgebung angeklickt werden. 
     */
    public void act() 
    {
        
        //continously moves the laser downwards
        setLocation(getX(), getY() - 7);
        
        //checks if laser has hit the world edge
                if (isAtEdge() == true) {
                    //removes object from world if touching the edge
                    getWorld().removeObject(this);
        }
    }    
}