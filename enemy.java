import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)
import java.util.Random;

/**
 * Ergänzen Sie hier eine Beschreibung für die Klasse enemy.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class enemy extends Actor
{
    Random rn = new Random();
    /**
     * 
     *
     */
    
    int enemyHP = 20;
    int laserCD = 0;
    
    //direction: [0-4 = left, 4-5- neutral,6-10 = right]
    int direction;
    int moveDuration;


        
    public void act() 
    {
        director director = getWorld().getObjects(director.class).get(0);
        player player = getWorld().getObjects(player.class).get(0);
        //moves the enemy down a bit every action
        setLocation(getX(), getY() + 1);
        

        //
        if (moveDuration < 0) {
            
            direction = rn.nextInt(11);
            moveDuration = rn.nextInt(76) + 30;
        }
        
        //System.out.println(direction);
        if (direction < 5) {
            setLocation(getX() - 1, getY());
        } else if (direction >= 5 && direction <= 7) {
            //dont move to the side
        } else if (direction > 7) {
            setLocation(getX() + 1, getY());
        } else {
            System.out.println(direction);
        }
        
        
        //changes direction of enemies if they get too close to border
        if (getX() < 5) {
            direction = 10;
        }
        
        if (getX() > 395) {
            direction = 0;
        }
        
        
        moveDuration -= 1;
        
        //checks if a laser has hit
        if (isTouching(playerLaser.class)) {
            removeTouching(playerLaser.class);
            enemyHP -= 10;
        }
        
        
        
        if (laserCD < 1) {
         //creates a laser object at player position
         
         Greenfoot.playSound("enemylaser.mp3");
         getWorld().addObject(new enemyLaser(), getX(), getY());
         int randomCD = (int)Math.floor(Math.random()*(150-55+1)+55);
         laserCD = randomCD;
     } else {
         
         //reduces cooldown by one
         laserCD -= 1;
     }
     
     
             if (isAtEdge() == true) {
                    //removes object from world if touching the edge

                   director.addCredits(360);
        
                    getWorld().removeObject(this);
        }
        
        
        if (this.enemyHP <= 0) {
            //gets the score and increases it by X using the setter
            int currentScore = player.getScore();
            player.setScore(currentScore + 20);
            director.setEnemiesDefeated(director.getEnemiesDefeated() + 1);
            //System.out.println(director.getEnemiesDefeated());
            
            getWorld().addObject(new enemyExplosion(), getX(), getY());
            //System.out.println("SUCCESS");
            getWorld().removeObject(this);
        }
        


        
    }    
}