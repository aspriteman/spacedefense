import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class HPbar here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class HPbar extends Actor
{
    /**
     * Act - do whatever the HPbar wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        player player = getWorld().getObjects(player.class).get(0);
        int playerHP = player.getHP();
        //Actor playerReference = getWorld().getObjects(player.class).get(0);
        //int playerHP = (player)((getWorld().getObjects(player.class)).get(0)).getHP();
        //int playerHP = playerReference.getHP();
        setImage(new GreenfootImage("HP: "+playerHP, 35, Color.RED, null));
    }
}
