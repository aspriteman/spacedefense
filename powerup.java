import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.HashMap;
import java.util.Random;

public class powerup extends Actor
{
    Random rn = new Random();
    //HashMap<Integer, String> powerupList = new HashMap<Integer, String>();
    //TODO: add hashmap with indexes for the names, e.g. with powerupList.put(1, "fastfire")
    
    //adds keys and values (index, powerup name)
    //powerupList.put(new Integer(1),"fastfire");
    boolean setupDone = false;
    int powerupNumber;
    String powerupName;
    
    
    
    public void act()
    {
        setLocation(getX(), getY() + 1);
        if (setupDone == false) {
        powerupNumber = rn.nextInt(5);
        setupDone = true;
    }
    
    
    switch (powerupNumber) {
        case 0:
            //doubleshot
            powerupName = "doubleshot";
            break;
        case 1:
            //fastfire
            powerupName = "fastfire";
            break;
        case 2:
            //life up
            powerupName = "lifeup";
            break;
        case 3:
            //shield
            powerupName = "shield";
            break;
        case 4:
            //speedup
            powerupName = "speedup";
            break;
        default:
            System.out.println("powerup error");
    }
    
    setImage("images/powerup_" + powerupName + ".png");
        
        //todo: randomly generate powerup type upon spawning, change powerup status and duration
        //upon touching
        if (isTouching(player.class) == true) {
            player player = getWorld().getObjects(player.class).get(0);
            player.setPowerupStatus(powerupNumber);
            Greenfoot.playSound("sounds/powerup.wav");
            getWorld().removeObject(this);
        } else if (isAtEdge() == true) {
            getWorld().removeObject(this);
        }
        
    }
}
