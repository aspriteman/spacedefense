import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;

/**
 * Ich weiß das Indenting ist ein Horror aber ich hatte echt damit zu kämpfen, die IDE ist mit nicht geheuer
 * 
 * 
 * 
 */
public class heavyEnemy extends Actor
{
    Random rn = new Random();
    
        int enemyHP = 50;
        int barrageCD = 0;
        int laserCD = 0;
        int shotsLeft = 4;
        int shotDisplacement;
    
        //direction: [0-4 = left, 4-5- neutral,6-10 = right]
        int direction;
        int moveDuration;
        
        //since classes can only move in whole numbers, 
        //to resemble decimal point movement speed dont move every x frames
        int movementCount = 0;
    

    
    public void act()
    {
        //this enemy is slightly slower than the regular one and shoots in bursts
        
        director director = getWorld().getObjects(director.class).get(0);
        player player = getWorld().getObjects(player.class).get(0);
       

        if (movementCount > 1) {
            movementCount = 0;
        } else {
            setLocation(getX(), getY() + 1);
            
            if (moveDuration < 0) {
            
            direction = rn.nextInt(11);
            moveDuration = rn.nextInt(76) + 30;
               }
               
            
        
            //System.out.println(direction);
            if (direction < 5) {
               setLocation(getX() - 1, getY());
               } else if (direction >= 5 && direction <= 7) {
               //dont move to the side
               } else if (direction > 7) {
               setLocation(getX() + 1, getY());
               } else {
               System.out.println(direction);
            }
            moveDuration -= 1;
            movementCount++;
        }
    
        //changes direction of enemies if they get too close to border
        if (getX() < 5) {
            direction = 10;
        }
        
        if (getX() > 395) {
            direction = 0;
        }
    
    
        if (isTouching(playerLaser.class)) {
            removeTouching(playerLaser.class);
            enemyHP -= 10;
        }
        
        //gatling laser - fires three shots per burst
        //outer
        
        if (barrageCD < 1) {
            
            if (laserCD < 1) {
                    Greenfoot.playSound("enemylaser.mp3");
                    //modulo - returns remainder after division (rest), checks if number is even
                    if ((shotsLeft % 2) == 0) {
                            // even
                            shotDisplacement = -10;
                        } else {
                            // odd
                            shotDisplacement = 10;
                        }
                    getWorld().addObject(new heavyEnemyLaser(), getX() + shotDisplacement, getY());
                    laserCD = 5;
                    shotsLeft -= 1;
            } else {
                laserCD -= 1;
            }
            
            //barrage cooldown reset if all shots are used up, shots get refilled
            if (shotsLeft < 1) {
                barrageCD = 200;
                shotsLeft = 4;
            }
        } else {
            barrageCD -= 1;
        }
        
        
        
        if (isAtEdge() == true) {
                    //removes object from world if touching the edge

                director.addCredits(650);
        
                getWorld().removeObject(this);
        }
        
                if (this.enemyHP <= 0) {
            //gets the score and increases it by X using the setter
            int currentScore = player.getScore();
            player.setScore(currentScore + 50);
            director.setEnemiesDefeated(director.getEnemiesDefeated() + 1);
            //System.out.println(director.getEnemiesDefeated());
            
            getWorld().addObject(new enemyExplosion(), getX(), getY());
            //System.out.println("SUCCESS");
            getWorld().removeObject(this);
        }
    
    }
}
