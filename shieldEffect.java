import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)

/**
 * Ergänzen Sie hier eine Beschreibung für die Klasse shieldEffect.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class shieldEffect extends Actor
{
    /**
     * Act - tut, was auch immer shieldEffect tun will. Diese Methode wird aufgerufen, 
     * sobald der 'Act' oder 'Run' Button in der Umgebung angeklickt werden. 
     */
    
    int shieldStatus = 0;
    
    public int getShieldStatus() {
        return shieldStatus;
    }
    
    public int setShieldStatus(int newShieldStatus) {
        shieldStatus = newShieldStatus;
        return shieldStatus;
    }
    
    public void act() 
    {
        player player = getWorld().getObjects(player.class).get(0);
        if (shieldStatus == 0) {
            setImage("images/director.png");
        } else if (shieldStatus == 1) {
            setImage("images/shield_effect.png");
            setLocation(player.getX(), player.getY());
        }
    }    
}