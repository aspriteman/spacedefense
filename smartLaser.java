import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;

/**
 * Write a description of class smartLaser here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class smartLaser extends Actor
{
    boolean directionSet = false;
    Random rn = new Random();
    int xOffset = rn.nextInt(21) - 10;
    int yOffset = rn.nextInt(21) - 10;
    
    public void act()
    {
        if (directionSet == false) {
            player player = getWorld().getObjects(player.class).get(0);
            
            turnTowards(player.getX() + xOffset, player.getY() + yOffset);
            directionSet = true;
        }
        
        move(3);
                
        if (isAtEdge() == true) {
                    //removes object from world if touching the edge
        getWorld().removeObject(this);
        }
    }
}
