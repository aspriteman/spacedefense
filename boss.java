import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;

/**
 * Write a description of class boss here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class boss extends Actor
{
    Random rn = new Random();
    int i;
    
    int bossHP = 1150;
    int movementCount = 0;
    int attackPattern;
    boolean bossValueSet = false;
    boolean attackPatternSet = false;
    
    int attackCD = 150;
    int CDoffset;
    int shotCD = 20;
    int shotsLeft;
    
    int[] shotCoords = new int[4];
    
    public void act()
    {
        if (movementCount > 11) {
            setLocation(getX(), getY() + 1);
            movementCount = 0;
        } else {
            movementCount++;
        }
        
        director director = getWorld().getObjects(director.class).get(0);
        director.setBossPresence(true);
        player player = getWorld().getObjects(player.class).get(0);
        
        
        if (attackCD < 1) {
            //generates pattern and sets parameters for incoming attack
            if (attackPatternSet == false) {
                attackPattern = rn.nextInt(3);
               
            switch (attackPattern) {
                case 0:
                    //triple shots straight down
                    shotsLeft = 3;
                    
                    shotCoords[0] = rn.nextInt(394) + 3;
                    shotCoords[1] = rn.nextInt(394) + 3;
                    shotCoords[2] = rn.nextInt(394) + 3;
                    break;
                case 1:
                    //diagonal laser attack
                    shotsLeft = 4;
                    break;
                case 2:
                    //smart laser
                    shotsLeft = 5;
                    break;
                case 3:
                    break;
            }
            attackPatternSet = true;
            }
            
            
            if (shotCD < 1) {
                    //Greenfoot.playSound("enemylaser.mp3");
                    
            switch (attackPattern) {
                case 0:
                    //triple shots straight down
                    shotCD = 10;
                    
                    Greenfoot.playSound("enemylaser_deep.mp3");
                    for (i = 0; i < 3; i++) {
                        getWorld().addObject(new enemyLaser(), shotCoords[i], getY()+ 50);
                    }
                    
                    shotsLeft -= 1;
                    break;
                case 1:
                //diagonal laser attack
                    
                    shotCD = 5;
                    
                    Greenfoot.playSound("enemylaser_deep.mp3");
                    
                    getWorld().addObject(new diagonalLaser(), rn.nextInt(394) + 3, getY()+ 50);
                    
                    shotsLeft -= 1;
                    break;
                case 2:
                //smart laser attack    
                    shotCD = 5;
                    
                    Greenfoot.playSound("enemylaser_deep.mp3");
                    
                    getWorld().addObject(new smartLaser(), rn.nextInt(394) + 3, getY()+ 50);
                    
                    shotsLeft -= 1;
                    break;
                case 3:
                    break;
            }
                    
            } else {
                shotCD -= 1;
            }
            
            //barrage cooldown reset if all shots are used up
            if (shotsLeft < 1) {
                CDoffset = rn.nextInt(51) - 25;
                attackCD = 200 + CDoffset;
                shotsLeft = 4;
                attackPatternSet = false;
            }
        } else {
            attackCD -= 1;
        }
        
        
        if (isTouching(playerLaser.class)) {
            removeTouching(playerLaser.class);
            bossHP -= 10;
        }
        
        

        if (isAtEdge() == true) {
                //game over upon reaching the bottom

                player.setHP(0);
        
                getWorld().removeObject(this);
        } else 
        if (this.bossHP <= 0) {
            //gets the score and increases it by X using the setter
            int currentScore = player.getScore();
            player.setScore(currentScore + 500);
            director.setEnemiesDefeated(director.getEnemiesDefeated() + 1);
            //System.out.println(director.getEnemiesDefeated());
            
            getWorld().addObject(new enemyExplosion(), getX(), getY());
            //System.out.println("SUCCESS");
            director.setBossesDefeated(director.getBossesDefeated() + 1);
            director.setBossPresence(false);
            getWorld().removeObject(this);
        }
        
    }
}
