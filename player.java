import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Write a description of class player here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class player extends Actor
{
    
    //TODO: indicators/sounds when you have a powerup, implement the rest
    
    int playerScore = 0;
    int laserCD = 0;
    int playerHP = 200;
    int playerSpeed = 2;
    int powerupStatus = 0;
    int previousPowerupStatus = 0;
    int powerupDuration = 0;
    int laserChargeTime = 20;
    int seriesCD = 0;
    int shotDisplacement = 0;
    int currentDisplacement = 0;
    int shotsLeftMax = 1;
    int shotsLeft = 1;
    int contactDmgCD = 20;
    boolean setupDone = false;
    
    int musicState = 1;
    int musicCD = 0;
    
    //enables godmode in testing
    int allowGodmode = 0;
    
    //reduces recieved damage by this value
    int defense = 0;
    int score; 
    
    GreenfootSound backgroundMusic = new GreenfootSound("sounds/music-the_arcade.mp3");

    //set of getters and setters - used outside of the player class to get/set a certain value
    public int getHP() {
        return playerHP;
    }
    
    public int setHP(int newHP) {
        playerHP = newHP;
        return playerHP;
    }
    
    public int getDefense() {
        return defense;
    }
    
    public int setDefense(int newDefense) {
        defense = newDefense;
        return defense;
    }
    
    public int getScore() {
        return playerScore;
    }
    
    public int setScore(int newScore) {
        playerScore = newScore;
        return playerScore;
    }
    
    public int getPowerupStatus() {
        return powerupStatus;
    }
    
    public int setPowerupStatus(int newStatus) {
        powerupStatus = newStatus;
        return powerupStatus;
    }
    
    public int getPowerupDuration() {
        return powerupDuration;
    }
    
    public int setPowerupDuration(int newDuration) {
        powerupDuration = newDuration;
        return powerupDuration;
    }
    
    public int getXpos() {
        return getX();
    }
    
    public int getYpos() {
        return getY();
    }
    
    
    /**
     * Act - do whatever the player wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
    
    if (setupDone == false) {
        //putting music in act prevents having it play before starting - link: https://www.youtube.com/watch?v=ypvFlsOetOA
        backgroundMusic.playLoop();
        setupDone = true;
    }
    
    if (Greenfoot.isKeyDown("V") && musicCD < 1) {
        if (musicState == 1) {
            backgroundMusic.stop();
            musicState = 0;
            musicCD = 120;
        } else {
            backgroundMusic.playLoop();
            musicState = 1;
            musicCD = 120;
        }
    } else {
        musicCD -= 1;
    }
        
    if (previousPowerupStatus != powerupStatus) {
        previousPowerupStatus = powerupStatus;
        powerupDuration = 500;
    }
    
    //checks if the powerup has expired - if not, reduce the duration counter
    if (powerupDuration < 1 && (powerupStatus != -1)) {
        powerupStatus = -1;
    } else {
        powerupDuration -= 1;
    }
    
    //checks if status has changes, resets duration if it did

    //switch statement that tracks which powerup (if any) is in effect
    
    shieldEffect shieldEffect = getWorld().getObjects(shieldEffect.class).get(0);
    switch (powerupStatus) {
        case -1:
        //default numbers
        laserChargeTime = 20;
        playerSpeed = 2;
        shotsLeftMax = 1;
        shotDisplacement = 0;
        defense = 0;
        shieldEffect.setShieldStatus(0);
            break;
        case 0:
        //doubleshot
        shotsLeftMax = 2;
        shotDisplacement = 10;
            break;
        case 1:
        //fastfire
            laserChargeTime = 9;
            break;
        case 2:
        //lifeup
             playerHP += 25;
             powerupStatus = -1;
            break;
        case 3:
        //shield
        defense = 3;
        shieldEffect.setShieldStatus(1);
            break;
        case 4:
        //speedup
            playerSpeed = 3;
            break;
        default:
            break;
            
    }
    //World world =getWorld();
    
        //movement code
            if (Greenfoot.isKeyDown("left")) {
            setLocation(getX() - playerSpeed, getY());
     }   
    
            if (Greenfoot.isKeyDown("right")) {
            setLocation(getX() + playerSpeed, getY());
     }
    
            if (Greenfoot.isKeyDown("down")) {
            setLocation(getX(), getY() + playerSpeed);
     }    
    
            if (Greenfoot.isKeyDown("up")) {
            setLocation(getX(), getY() - playerSpeed);
     }
     
     
     
    //godmode - dev function to test things by not dying
     
    if (Greenfoot.isKeyDown("G") && Greenfoot.isKeyDown("O") && Greenfoot.isKeyDown("D") && allowGodmode == 1) {
        playerHP = 9999;
        defense = 50;
        powerupStatus = 0;
    }
     
    // OLD shooting code, if not on cooldown perform action
     
    // if (Greenfoot.isKeyDown("space") && (laserCD < 1)) {
    //     //creates a laser object at player position
         
    //     Greenfoot.playSound("laser.mp3");
    //     getWorld().addObject(new playerLaser(), getX(), getY());
    //     laserCD = laserChargeTime;
    // } else {
         
         //reduces cooldown by one
    //     laserCD -= 1;
    // }
     
     
     
    if (Greenfoot.isKeyDown("space") && laserCD < 1) {
            
            if (laserCD < 1) {
                    Greenfoot.playSound("laser.mp3");
                    //modulo - returns remainder after division (rest), checks if number is even
                    if ((shotsLeft % 2) == 0) {
                            // even
                            currentDisplacement = shotDisplacement * -1;
                        } else {
                            // odd
                            currentDisplacement = shotDisplacement;
                        }
                    getWorld().addObject(new playerLaser(), getX() + currentDisplacement, getY());
                    seriesCD = 0;
                    shotsLeft -= 1;
            } else {
                seriesCD -= 1;
            }
            
            //barrage cooldown reset if all shots are used up, shots get refilled
            if (shotsLeft < 1) {
                laserCD = laserChargeTime;
                shotsLeft = shotsLeftMax;
            }
        } else {
            laserCD -= 1;
        }
     
        //checks if a laser has hit the player
        if (isTouching(enemyLaser.class)) {
            removeTouching(enemyLaser.class);
            playerHP -= 10 - defense;
        }
        
        if (isTouching(heavyEnemyLaser.class)) {
            removeTouching(heavyEnemyLaser.class);
            playerHP -= 5 - defense;
        }
        
        if (isTouching(diagonalLaser.class)) {
            removeTouching(diagonalLaser.class);
            playerHP -= 8 - defense;
        }
        
        if (isTouching(smartLaser.class)) {
            removeTouching(smartLaser.class);
            playerHP -= 10 - defense;
        }
        
        //contact damage, negated by shield
        if ((isTouching(enemy.class) || isTouching(heavyEnemy.class) || isTouching(boss.class)) && contactDmgCD < 1) {
            
            if (defense == 0) {
                playerHP -= 1;
            }
            contactDmgCD = 20;
        } else {
            contactDmgCD -= 1;
        }

        
        if (playerHP <= 0) {
            //removes player from world
            getWorld().addObject(new enemyExplosion(), getX(), getY());
            getWorld().addObject(new gameOverScreen(), 200, 300);
            backgroundMusic.stop();
            //getWorld().removeObject(this);
            setImage("images/director.png");
            Greenfoot.stop();
        }
        
    }
}
