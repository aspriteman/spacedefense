import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot und MouseInfo)

/**
 * Ergänzen Sie hier eine Beschreibung für die Klasse enemyExplosion.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class enemyExplosion extends Actor
{
    GifImage gifImage = new GifImage("enemyExplosion.gif");
    int timer = 65;
    /**
     * Act - tut, was auch immer enemyExplosion tun will. Diese Methode wird aufgerufen, 
     * sobald der 'Act' oder 'Run' Button in der Umgebung angeklickt werden. 
     */
    public void act() 
    {
     setImage(gifImage.getCurrentImage());
     if (timer < 1) {
         getWorld().removeObject(this);
    } else {
        timer--;
    }
    }
}