import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;

/**
 * The director - a single pixel that spawns an enemy where ever it randomly
 * teleports 
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class director extends Actor
{
    /**
     * credits - the director uses these to determine when to place an enemy
     * and uses score to choose how to spend them
     */
    Random rn = new Random();
    int credits = 0;
    int powerupCooldown = 700;
    int randomPowerupCD;
    int enemiesDefeated = 0;
    int score;
    int enemyTypeNumber;
    int enemyTypeIndex;
    
    //furthest the random number generator is allowed to generate into the type array
    int enemyTypeMax = 2;
    
    //player player = getWorld().getObjects(player.class).get(0);
    
    int normalEnemyCost = 300;
    int heavyEnemyCost = 360;
    
    boolean bossPresence = false;
    int bossesSpawned = 0;
    int bossesDefeated = 0;
    
    
    
    // maximum number generated from array gets changed depending on score/enemies
    // killed. example: 0-1 until score 200, then max increases to include 0-2 for added difficulty
    int[] enemyTypeArray = {1, 1, 2};
    
    public int getCredits () {
        return credits;
    }
    
    public int setCredits (int newCredits) {
        credits = newCredits;
        return credits;
    }
    
    public int addCredits (int creditAmount) {
        credits = credits + creditAmount;
        return credits;
    }
    
    public int getEnemiesDefeated () {
        return enemiesDefeated;
    }
    
    public int setEnemiesDefeated (int newCount) {
        enemiesDefeated = newCount;
        return enemiesDefeated;
    }
    
    public boolean getBossPresence () {
        return bossPresence;
    }
    
    public boolean setBossPresence (boolean newValue) {
        bossPresence = newValue;
        return bossPresence;
    }
    
    public int getBossesDefeated () {
        return bossesDefeated;
    }
    
    public void setBossesDefeated (int newBossValue) {
        bossesDefeated = newBossValue;
    }
    
    void randomTeleport() {
        int randomX = (int)Math.floor((Math.random()*(390-10+1)+10));
        int randomY = (int)Math.floor(Math.random()*(20+1));
        setLocation(randomX, randomY);
    }
    /**
     * Act - do whatever the director wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        
    //updates score every frame
    player player = getWorld().getObjects(player.class).get(0);
    
    score = player.getScore();
    
    
    //allows more enemy types to spawn/different weighting
    //idea: cost reduction at higher score for increased spawn rate  enemyTypeArray[enemyTypeIndex]
    if(score < 200) {
        enemyTypeMax = 2;
        normalEnemyCost = 200;
        heavyEnemyCost = 360;
    } else
    if (score >= 200 && score < 1200) {
        enemyTypeMax = 3;
        normalEnemyCost = 180;
        heavyEnemyCost = 360;
    } else
    if (score >= 1200 && score < 2500) {
        normalEnemyCost = 160;
        heavyEnemyCost = 300;
    }
    if (score >= 2500 && score < 5500) {
        normalEnemyCost = 120;
        heavyEnemyCost = 270;
    } else 
    if (score >= 5500) {
        normalEnemyCost = 90;
        heavyEnemyCost = 200;
    }
    
    
    if (bossPresence == true) {
        normalEnemyCost = 500;
        heavyEnemyCost = 800;
    }
    
    
    //boss spawning
    if (player.getScore() >= 1000 + 1500 * bossesSpawned) {
        getWorld().addObject(new boss(),200, 1);
        bossesSpawned += 1;
        credits = -600;
        bossPresence = true;
    }
    
    
        if (credits > 200) {
            randomTeleport();
            //adds a new enemy at the director's location
            //random number taken from array
             
            enemyTypeIndex = rn.nextInt(enemyTypeMax);   
            enemyTypeNumber = enemyTypeArray[enemyTypeIndex];
            
            switch(enemyTypeNumber) {
                case 1:
                    getWorld().addObject(new enemy(), getX(), getY());
                    credits -= normalEnemyCost;
                    //System.out.println(normalEnemyCost);
                    break;
                case 2:
                    getWorld().addObject(new heavyEnemy(), getX(), getY());
                    credits -= heavyEnemyCost;
                    break;
                default:
                    System.out.println("director error");
                
            }
            int randomCredits = (int)Math.floor(Math.random()*(65-(-70)+1)-70);
            credits = credits + randomCredits;
        } else {
            // increases credits by one
            credits += 1;
        }
        
        if (powerupCooldown < 1) {
            randomTeleport();
            getWorld().addObject(new powerup(), getX(), getY());
            randomPowerupCD = (int)Math.floor(Math.random()*(200-(-200)+1)-200);
            powerupCooldown = 1200 + randomPowerupCD;
        } else {
            powerupCooldown -= 1;
        }
    }
    
}
