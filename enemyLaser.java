import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class enemyLaser here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class enemyLaser extends Actor
{
    /**
     * Act - do whatever the enemyLaser wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        
                setLocation(getX(), getY() + 5);
        
                if (isAtEdge() == true) {
                    //removes object from world if touching the edge
                    getWorld().removeObject(this);
        }
        
        
    }
}
