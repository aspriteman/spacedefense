import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class scorebar here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class scorebar extends Actor
{
    /**
     * Act - do whatever the scorebar wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        player player = getWorld().getObjects(player.class).get(0);
        int playerScore = player.getScore();
        // Add your action code here.
        setImage(new GreenfootImage("Score: "+playerScore, 35, Color.WHITE, null));
    }
}
